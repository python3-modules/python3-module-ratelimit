%define _unpackaged_files_terminate_build 1
%define pypi_name ratelimit

%def_with check

Name: python3-module-%pypi_name
Version: 2.2.1
Release: alt1
Summary: Decorator for limiting API call rate to prevent exceeding provider's rate limits
Group: Development/Python3
License: MIT
Url: https://pypi.org/project/ratelimit
Vcs: https://github.com/tomasbasham/ratelimit.git
Source0: %name-%version.tar

BuildRequires(pre): rpm-build-pyproject
BuildRequires: python3(setuptools)
BuildRequires: python3(wheel)

%if_with check
BuildRequires: python3(pytest)
BuildRequires: python3(pytest-cov)
%endif

BuildArch: noarch

%description
This package provides a decorator to control the rate of function calls, aligning with API rate limits to avoid application bans. It's essential for applications that interact heavily with external APIs, which often impose call frequency restrictions. The ratelimit decorator allows specifying the maximum number of calls allowed within a given period, effectively managing access to rate-limited resources. By ensuring compliance with API rate limits, it helps maintain uninterrupted service access and avoid penalties for excessive requests. This tool is crucial for developers aiming to implement respectful and sustainable API consumption practices.

%prep
%setup

%build
%pyproject_build

%install
%pyproject_install

%check
%pyproject_run_pytest tests

%files
%doc README.rst LICENSE.txt
%python3_sitelibdir/%pypi_name/
%python3_sitelibdir/%{pyproject_distinfo %pypi_name}

%changelog
* Wed Apr 10 2024 Aleksandr A. Voyt <vojtaa@basealt.ru> 2.2.1-alt1
- First package version

